# BP LiL GA

A LinkedIn Learning and getAbstract widget for all informal learning sites within BP.

This project uses [Vue Custom Element](https://github.com/karol-f/vue-custom-element) to turn a Vue.js component into a web component.

## Development

1. `yarn run dev`
2. Open `demo/index.html` in a browser
3. Edit `src/main.js` or `src/components/LiLGA.vue`
4. Refresh the page

