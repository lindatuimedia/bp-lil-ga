import buble from 'rollup-plugin-buble'
import gzip from 'rollup-plugin-gzip'
import nodeResolve from 'rollup-plugin-node-resolve'
import replace from 'rollup-plugin-replace'
import uglify from 'rollup-plugin-uglify'
import vue from 'rollup-plugin-vue'

export default {
  entry: 'src/main.js',
  dest: 'dist/bp-lil-ga.js',
  plugins: [
    replace({
      'process.env.NODE_ENV': JSON.stringify('production')
    }),
    nodeResolve({
      module: true,
      jsnext: true,
      main: true
    }),
    vue({
      compileTemplate: true,
      css: true
    }),
    buble(),
    uglify(),
    gzip()
  ]
}
