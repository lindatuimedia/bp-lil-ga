// Custom Elements polyfill for IE 9+
import 'document-register-element/build/document-register-element'
import Vue from 'vue'
import VueCustomElement from 'vue-custom-element'
import BpLiLGA from './components/LiLGA.vue'

Vue.use(VueCustomElement)

Vue.customElement('bp-lil-ga', BpLiLGA)
